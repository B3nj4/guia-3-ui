#ifndef LISTAA_H
#define LISTAA_H

//Librerias
#include <iostream>
using namespace std;

#include "Numero.h"

/* define la estructura del nodo. */
typedef struct _Nodo {
    Numero *numero;
    struct _Nodo *sgte;
} Nodo;

class Listaa {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;
        int mayor = 0;
        int menor = 0;

    public:
        /* constructor*/
        Listaa();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Numero. */
        void agregar(Numero *numero);
        /* imprime la lista. */
        void mostrar();
        void MenorMayor();  //para ver que numero de la lista es el mayor y el menor
        int get_Mayor();  
        int get_Menor();   
        void vaciarLista(); 
        void ejecutarvacio();
};
#endif
