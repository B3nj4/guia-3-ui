#ifndef NUMERO_H
#define NUMERO_H

//Librerias
#include <iostream>
using namespace std;

class Numero {
    private:
        int numero = 0;

    public:
        /* constructor */
        Numero(int numero);
        
        /* métodos get */
        int get_numero();
};
#endif
