/* Considere que tiene una lista enlazada simple de número enteros, ordenados crecientemente (Lea los
datos). Donde pueden existir valores no correlativos (ejemplo: 10 - 11 - 15 - 16 - 20). Escriba un
programa que complete la lista, de tal manera que la misma, una vez modificada almacene todos los
valores a partir del número del primer nodo hasta el último número del último nodo. Para el ejemplo,
la lista guardará los números: 10 - 11 - 12 - 13 - 14 - 15 - 16 - 17 - 18 - 19 - 20.
*/

//Librerias
#include <iostream>
using namespace std;

//Clases
#include "Numero.h"
#include "Listaa.h"

// Menu de opciones
int menu(){
    string opc;

    cout << "----------------" << endl;
    cout << "Agregar número [1]" << endl;
    cout << "Modificar      [2]" << endl;
    cout << "Salir          [0]" << endl;
    cout << "----------------" << endl;
    cout << "Opcion: ";
    cin >> opc;
    
  return stoi(opc);
}

// Funcion para rellenar con numeros la lista para que sean consecutivos
void modificarlista(Listaa lista,int n) {
    //instanciar funcion para definir quien es el mayor y el menor
    lista.MenorMayor();
    int aux_mayor = lista.get_Mayor();
    int aux_menor = lista.get_Menor();
    
    //se vacia la lista
    lista.vaciarLista();
    
    //se rellena la lista
    for (int i=aux_menor; i<=aux_mayor; i++){
        Numero *numero = new Numero(i);
        lista.agregar(numero);
    }
    
    //imprime la lista
    lista.mostrar();
}

// Funcion principal del programa
int main(void) {
    
    // Objeto lista
    Listaa *lista = new Listaa();
    
    //Variables
    int opc = 0;
    int line;

    system("clear");

    //Interactuar con el menu
    do {
        opc = menu();

        switch (opc) {
            //Agregar numero y mostrar informacion
            case 1:
                cout << "Valor: ";
                cin >> line;
                lista->agregar(new Numero(line));
                lista->mostrar();
                break;

            case 2:
                //Funcion para modificar la lista
                modificarlista(*lista,line);
        }

    } while (opc != 0); // Sale del programa

    return 0;
}
