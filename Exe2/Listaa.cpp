//Librerias
#include <iostream>
using namespace std;

#include "Listaa.h"

Listaa::Listaa() {}

void Listaa::agregar(Numero *numero) {
    Nodo *tmp;

    /* crea un nodo . */ 
    tmp = new Nodo;

    /* asigna la instancia de Numero. */
    tmp->numero = numero;
    
    /* apunta a NULL por defecto. */
    tmp->sgte = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo 
    como el último de la lista. */
    } else {
        this->ultimo->sgte = tmp;
        this->ultimo = tmp;
    }
}

void Listaa::mostrar() {
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << "\n";
    while (tmp != NULL) {
        cout << "Numero: " << tmp->numero->get_numero() << endl;
        tmp = tmp->sgte;
    }
}

