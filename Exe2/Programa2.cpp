/* Escriba un programa que genere dos listas enlazadas ordenadas (lea los datos) y forme una tercera
lista que resulte de la mezcla de los elementos de ambas listas. Muestre el contenido finalmente de las
tres listas.
*/

//Librerias
#include <iostream>
using namespace std;

//Clases
#include "Numero.h"
#include "Listaa.h"

// Menu de opciones
int menu(int numero){
    string opc;

    cout << "\n----------------" << endl;
    cout << "Lista " << numero << endl;
    cout << "----------------" << endl;
    cout << "Agregar número [1]" << endl;
    cout << "Salir          [2]" << endl;
    cout << "----------------" << endl;
    cout << "Opcion: ";
    cin >> opc;
    
  return stoi(opc);
}

// Funcion para mezclar dos listas ingresadas por el usuario
void mezclar(Listaa *final, Listaa *lista){
    
    //creacion de nodo auxiliar
    Nodo *tmd;
    tmd = lista->raiz;
    // proceso que agregara los elementos y punteros necesarios para la lista3
    while(tmd != NULL){
        final->agregar(tmd->numero);
        tmd = tmd->sgte;
    }

}

void crear_lista(Listaa *lista, int numero){
     //Variables
    int opc = 0;
    int line;

    system("clear");

    //Interactuar con el menu
    do {
        opc = menu(numero);

        switch (opc) {
            //Agregar numero y mostrar informacion
            case 1:
                cout << "Valor: ";
                cin >> line;
                lista->agregar(new Numero(line));
                //agregar(Numero numero);
                lista->mostrar();
                break;
        }
    } while (opc != 2); // Sale del programa
}

// Funcion principal del programa
int main(void) {
    
    // Objeto lista
    Listaa *lista1 = new Listaa();
    Listaa *lista2 = new Listaa();

    // Creacion de lista1 y lista2
    crear_lista(lista1,1);
    crear_lista(lista2,2);

    // Mezclar datos de las listas 1 y 2 
    Listaa *lista3 = new Listaa();

    // Se fusionan las listas 1 y 2 a la lista 3
    mezclar(lista3, lista1);
    mezclar(lista3, lista2);

    // Se meustra la lista3
    lista3->mostrar();

    //liberar memoria
    delete lista1;
    delete lista2;

    return 0;
}
