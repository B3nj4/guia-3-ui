/* Escriba un programa que solicite números enteros y los vaya ingresando a una lista enlazada simple
ordenada. Por cada ingreso debe mostrar el estado de la lista. 
*/

//Librerias
#include <iostream>
using namespace std;

//Clases
#include "Numero.h"
#include "Listaa.h"

// Menu
int menu(){
    string opc;

    cout << "----------------" << endl;
    cout << "Agregar número [1]" << endl;
    cout << "Salir          [2]" << endl;
    cout << "----------------" << endl;
    cout << "Opcion: ";
    cin >> opc;
    
  return stoi(opc);
}

// Funcion principal del programa
int main(void) {
    
    // Objeto lista
    Listaa *lista = new Listaa();
    
    //Variables
    int opc = 0;
    int line;

    system("clear");

    //Interactuar con el menu
    do {
        opc = menu();

        switch (opc) {
            //Agregar numero y mostrar informacion
            case 1:
                cout << "Valor: ";
                cin >> line;
                lista->agregar(new Numero(line));
                //agregar(Numero numero);
                lista->mostrar();
                break;
        }
    } while (opc != 2); // Sale del programa

    return 0;
}
