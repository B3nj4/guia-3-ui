#ifndef LISTAA_H
#define LISTAA_H

//Librerias
#include <iostream>
using namespace std;

#include "Numero.h"

/* define la estructura del nodo. */
typedef struct _Nodo {
    Numero *numero;
    struct _Nodo *sgte;
} Nodo;

class Listaa {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Listaa();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Numero. */
        void agregar(Numero *numero);
        /* imprime la lista. */
        void mostrar();
};
#endif
